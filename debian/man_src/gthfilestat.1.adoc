# gthfilestat(1)

## NAME

gthfilestat - show statistics about consensus spliced alignments

## SYNOPSIS

*gthfilestat* [option ...] [file ...]

## DESCRIPTION

Show statistics about spliced alignments in GenomeThreader output files
containing intermediate results.

## OPTIONS

*-minalignmentscore*::
  set the minimum alignment score for spliced alignments to be
                   included into the set of spliced alignments
                   default: 0.00

*-maxalignmentscore*::
  set the maximum alignment score for spliced alignments to be
                   included into the set of spliced alignments
                   default: 1.00

*-mincoverage*::
  set the minimum coverage for spliced alignments to be
                   included into the set of spliced alignments
                   default: 0.00

*-maxcoverage*::
  set the maximum coverage for spliced alignments to be
                   included into the set of spliced alignments
                   default: 9999.99

*-v*::
  be verbose
                   default: no

*-help*::
  display help and exit

*-version*::
  display version information and exit
