# gthbssmfileinfo(1)

## NAME

gthbssmfileinfo - show info about splice site model

## SYNOPSIS

*gthbssmfileinfo* [option ...] bssm_file

## DESCRIPTION

Show information about the specified BSSM file.

## OPTIONS

*-help*::
 display help and exit

*-version*::
 display version information and exit
