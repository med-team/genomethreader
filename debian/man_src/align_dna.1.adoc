# align_dna(1)

## NAME

align_dna - spliced align genomic sequence with cDNA

## SYNOPSIS

*align_dna* [option ...] -datapath dir -bssmfile file

## OPTIONS

*-genreverse*::
  align reverse strand of genomic sequence
            default: no

*-genrange*::
  set aligned genomic range
            default: undefined

*-help*::
  display help and exit

*-version*::
  display version information and exit
