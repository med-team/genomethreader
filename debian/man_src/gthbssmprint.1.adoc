# gthbssmprint(1)

## NAME

gthbssmprint - print splice site model

## SYNOPSIS

*gthbssmprint* [option ...] bssm_file

## DESCRIPTION

Print BSSM file bssm_file to stdout.

## OPTIONS

*-help*::
 display help and exit

*-version*::
 display version information and exit
